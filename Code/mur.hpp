#ifndef MUR_HPP
#define MUR_HPP
#include <utility>
#include<vector>

struct Mur {
  std::pair<unsigned int,unsigned int> case_1;
  std::pair<unsigned int,unsigned int> case_2;

  Mur (std::pair<unsigned int,unsigned int> p1,
      std::pair<unsigned int,unsigned int> p2) {
        case_1 = std::pair<unsigned int,unsigned int>(p1);
        case_2 = std::pair<unsigned int,unsigned int>(p2);
  }

};


class Walls{

public:

  std::vector<Mur> tab;

  Walls(int largeur, int hauteur);

  std::vector<Mur> tab_murs(int largeur,int hauteur);

  void melanger();

};






#endif
