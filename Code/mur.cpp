#include <algorithm>
#include "mur.hpp"

Walls::Walls(int hauteur, int largeur) {
  std::pair<unsigned int,unsigned int> p;
  Mur mur1(p,p);
  Mur mur2(p,p);
  for(int i = 0; i<hauteur; i++){
    for(int j = 0; j<largeur; j++){
      if(j < largeur - 1){
        mur1.case_1.first = i;
        mur1.case_1.second = j;
        mur1.case_2.first = i;
        mur1.case_2.second = j+1;
        tab.push_back(mur1);
      }
      if(i < hauteur - 1){
        mur2.case_1.first = i;
        mur2.case_1.second = j;
        mur2.case_2.first = i+1;
        mur2.case_2.second =j;
        tab.push_back(mur2);
      }
    }
  }
}



void Walls::melanger() {
  random_shuffle(tab.begin(),tab.end());
}
