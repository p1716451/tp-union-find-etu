#ifndef UNION_FIND
#define UNION_FIND
#include <vector>

class Arbre_uf{

private:
  int taille;
  std::vector<int> tab;


public:
  Arbre_uf(int t);

  int parent(int noeud) const;

  bool recherche(int noeud1,int noeud2) const;

  void union_arbre(int noeud1, int noeud2);




};



#endif
