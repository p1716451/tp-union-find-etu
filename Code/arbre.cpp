
#include "arbre.hpp"

Arbre_uf:: Arbre_uf(int t){
  taille = t;
  for (int i = 0; i <taille; i++){
    tab.push_back(i);
  }
}


int Arbre_uf::parent(int noeud)const{
  int pere = tab[noeud];
  while(pere != tab[pere]){
    pere = tab[pere];
  }
  return pere;
}

bool Arbre_uf::recherche(int noeud1,int noeud2)const{
  int pere1 = parent(noeud1);
  int pere2 = parent(noeud2);
  return (pere1 == pere2);
}


void Arbre_uf::union_arbre(int noeud1, int noeud2){

  if (!recherche(noeud1,noeud2)){
    int pere1 = parent(noeud1);
    int pere2 = parent(noeud2);

    tab[pere1] = pere2;
  }
}
